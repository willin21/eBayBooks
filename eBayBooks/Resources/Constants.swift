//
//  Constants.swift
//  eBayBooks
//
//  Created by wilnin on 3/09/21.
//

import Foundation

struct Constants {
    struct RequestDomain {
        static let baseURL = "https://de-coding-test.s3.amazonaws.com/"
    }

    struct Titles {
        static let bookAlert = "Something was wrong"
        static let ok = "Ok"
        static let loading = "Loading..."
    }
}
