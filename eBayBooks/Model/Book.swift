//
//  Book.swift
//  eBayBooks
//
//  Created by wilnin on 4/09/21.
//

import Foundation

struct Book {
    let title: String?
    let author: String?
    let imageURL: String?
}

struct Cells {
    static let Book = "BookTableViewCell"
}
