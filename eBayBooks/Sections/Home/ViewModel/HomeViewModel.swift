//
//  HomeViewModel.swift
//  eBayBooks
//
//  Created by wilnin on 4/09/21.
//

import Foundation

class HomeViewModel {
    private let repository = BookRepository()
    var delegate: BooksProtocol?

    func callBooksRepository() {
        repository.getBooksApi { result in
            switch result {
            case .failure(let error):
                self.delegate?.showAlert(error: error.localizedDescription)
            case .success(let data):
                let books = data.map { item -> Book in
                    let book = Book(title: item.title, author: item.author, imageURL: item.imageURL)
                    return book
                }

                self.delegate?.loadData(data: books)
            }
        }
    }
}
