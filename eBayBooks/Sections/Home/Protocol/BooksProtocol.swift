//
//  BooksProtocol.swift
//  eBayBooks
//
//  Created by wilnin on 4/09/21.
//

import Foundation

protocol BooksProtocol {
    func loadData(data: [Book])
    func showAlert(error: String)
}
