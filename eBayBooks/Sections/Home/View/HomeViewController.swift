//
//  ViewController.swift
//  eBayBooks
//
//  Created by wilnin on 3/09/21.
//

import UIKit
import MBProgressHUD

class HomeViewController: UIViewController {
    @IBOutlet var booksTableView: UITableView!

    var hud: MBProgressHUD?
    private var bookViewModel = HomeViewModel()
    private var data: [Book] = [] {
        didSet{
            booksTableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        bookViewModel.delegate = self
        cellRegister()
        doRequest()
    }

    func doRequest() {
        showLoading()
        bookViewModel.callBooksRepository()
    }

    func cellRegister() {
        booksTableView.register(UINib(nibName: Cells.Book, bundle: nil), forCellReuseIdentifier: Cells.Book)
    }

    func alert(_ error: String) {
        let alert = UIAlertController(title: Constants.Titles.bookAlert, message: error, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Constants.Titles.ok, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    func showLoading() {
        hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud?.label.text = Constants.Titles.loading
    }

    func hideLoading() {
        hud?.hide(animated: true)
    }
}

extension HomeViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = booksTableView.dequeueReusableCell(withIdentifier: Cells.Book) as? BookTableViewCell
        cell?.setupCell(data: data[indexPath.row])
        return cell!
    }
}

extension HomeViewController: UITableViewDelegate { }

extension HomeViewController: BooksProtocol {
    func loadData(data: [Book]) {
        self.data = data
        hideLoading()
    }

    func showAlert(error: String) {
        self.alert(error)
        hideLoading()
    }
}

