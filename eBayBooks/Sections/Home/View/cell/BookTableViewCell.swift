//
//  BookTableViewCell.swift
//  eBayBooks
//
//  Created by wilnin on 4/09/21.
//

import UIKit
import SDWebImage

class BookTableViewCell: UITableViewCell {

    @IBOutlet weak var bookImage: UIImageView!
    @IBOutlet weak var bookTitleLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setupCell(data: Book) {
        let urlImage = URL(string: data.imageURL ?? "")
        bookImage.sd_setImage(with: urlImage, placeholderImage: UIImage(named: "book"))
        bookTitleLabel.text = data.title

        if let author = data.author {
            authorLabel.text = author
            authorLabel.isHidden = false
        } else {
            authorLabel.isHidden = true
        }

    }
    
}
