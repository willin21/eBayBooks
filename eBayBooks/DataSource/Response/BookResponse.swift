//
//  BookResponse.swift
//  eBayBooks
//
//  Created by wilnin on 3/09/21.
//

import Foundation

struct BookResponse: Codable {
    let title: String?
    let author: String?
    let imageURL: String?

    enum CondingKeys: String, CodingKey {
        case title
        case author
        case imageURL
    }
}
