//
//  BookRepository.swift
//  eBayBooks
//
//  Created by wilnin on 3/09/21.
//

import Foundation
import Alamofire

class BookRepository: NetworkManager {
    func getBooksApi(completion: @escaping(_ result: Result<[BookResponse]>) -> Void) {
        let api = ApiRouter.book
        callService(api, completion: completion)
    }
}
