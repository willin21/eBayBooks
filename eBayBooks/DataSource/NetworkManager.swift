//
//  NetworkManager.swift
//  eBayBooks
//
//  Created by wilnin on 3/09/21.
//

import Alamofire
import Foundation

enum Result<T> {
    case success(T)
    case failure(NSError)
}

class NetworkManager {
    private let session: SessionManager

    init() {
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = 15
        config.timeoutIntervalForResource = 15
        session = Alamofire.SessionManager(configuration: config)
    }

    func callService<T: Codable>(_ api: URLRequestConvertible, completion: @escaping (_ result: Result<T>) -> Void) {

        session
            .request(api)
            .validate()
            .responseData { (response) in
                switch response.result {
                case .success(let data):
                    do {
                        let model: T = try T.parseModel(data: data)
                        completion(Result.success(model))
                    } catch {
                        let nserror = NSError(domain: "Parsing data error.", code: response.response?.statusCode ?? 0, userInfo: ["message": "Se ha presentado un error."])
                        completion(Result.failure(nserror))
                    }
                case .failure:
                    let nserror = NSError(domain: "Parsing data error.", code: response.response?.statusCode ?? 0, userInfo: ["message": "Se ha presentado un error."])
                    completion(Result.failure(nserror))
                }
            }
    }
}

extension Decodable {
    static func parseModel<T: Codable>(data: Data) throws -> T {
        let decoder = JSONDecoder()
        let model = try decoder.decode(T.self, from: data)
        return model
    }
}
