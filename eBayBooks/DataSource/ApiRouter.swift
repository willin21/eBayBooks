//
//  ApiRouter.swift
//  eBayBooks
//
//  Created by wilnin on 3/09/21.
//

import Foundation
import Alamofire

enum ApiRouter: URLRequestConvertible {
    case book

    var absoluteUrl: String {
        switch self {
        default:
            return getBaseDomain()
        }
    }

    func getBaseDomain() -> String {
        switch self {
        default:
            return Constants.RequestDomain.baseURL + path
        }
    }

    var path: String {
        switch self {
        case .book:
            return "books.json"
        }
    }

    var headers: HTTPHeaders {
        switch self {
        case .book:
            return [:]
        }
    }

    var method: HTTPMethod {
        switch self {
        case .book:
            return .get
        }
    }

    var params: [String: Any] {
        switch self {
        case .book:
            return [:]
        }
    }

    var encoding: ParameterEncoding {
        switch self {
        case .book:
            return URLEncoding.queryString
        }
    }

    func asURLRequest() throws -> URLRequest {
        let url = try absoluteUrl.asURL()
        var urlRequest = try URLRequest(url: url, method: method, headers: headers)
        urlRequest = try encoding.encode(urlRequest, with: params)

        return urlRequest
    }
}
